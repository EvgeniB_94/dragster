import processing.sound.*;

int[] distance = new int[] {0, 0};
final int maxDistance = 256*97, maxTach = 32, maxGear = 4;
int[] speed = new int[] {0, 0};
int[] gear = new int[] {0, 0};
int[] tach = new int[] {0, 0};
final int[] maxSpeed = new int[] {0, 32, 64, 126, 252};
int[] currentMaxSpeed = new int[] {0, 0};
boolean[] neutral = new boolean[] {true, true},
clutch = new boolean[] {false, false},
shift = new boolean[] {false, false},
gas = new boolean[] {false, false};
char[] currentState = new char[] {'N', 'N'};
float[] counter = new float[] {0.0, 0.0};
int startTime = 0;
boolean[] playerFinish = new boolean[] {false, false};
int countDown = 9;
boolean started = false;
boolean[] early = new boolean[] {false, false},
blown = new boolean[] {false, false};
int elapsed = 0;
boolean[] stall = new boolean[] {false, false};
int frameCounter = 1;
int[] dragsterFrame = new int[] {0, 0};
final int[] frameReset = new int[] {1, 1, 2, 4, 8};
PImage bgImg;
PImage[] dragster = new PImage[3];
SoundFile beepFile, bgFile, xplosionFile;
SoundFile[] shiftFile = new SoundFile[2];
SoundFile[][] gearFile = new SoundFile[2][5];
boolean blownSound = false;

void setup() {
  frameRate(30);
  
  size(512, 448);
  background(0);
  noStroke();
  
  bgImg = loadImage("Background.png");
  dragster[0] = loadImage("Dragster1.png");
  dragster[1] = loadImage("Dragster2.png");
  dragster[2] = loadImage("Dragster3.png");
  
  beepFile = new SoundFile(this, "Beep.mp3");
  bgFile = new SoundFile(this, "BgSound.mp3");
  xplosionFile = new SoundFile(this, "Xplosion.mp3");
  
  shiftFile[0] = new SoundFile(this, "Shift1.mp3");
  shiftFile[1] = new SoundFile(this, "Shift2.mp3");
  
  gearFile[0][0] = new SoundFile(this, "Gear0_1.mp3");
  gearFile[0][1] = new SoundFile(this, "Gear1_1.mp3");
  gearFile[0][2] = new SoundFile(this, "Gear2_1.mp3");
  gearFile[0][3] = new SoundFile(this, "Gear3_1.mp3");
  gearFile[0][4] = new SoundFile(this, "Gear4_1.mp3");
  
  gearFile[1][0] = new SoundFile(this, "Gear0_2.mp3");
  gearFile[1][1] = new SoundFile(this, "Gear1_2.mp3");
  gearFile[1][2] = new SoundFile(this, "Gear2_2.mp3");
  gearFile[1][3] = new SoundFile(this, "Gear3_2.mp3");
  gearFile[1][4] = new SoundFile(this, "Gear4_2.mp3");
  
  bgFile.loop();
}

void draw() {
  image(bgImg, 0, 0);
  
  textSize(16);
  fill(0);
  
  if (!early[0] && !early[0] && !blown[0] && !blown[1])
  {
    updateState();
  } 
  
  if (!blown[0] && !blown[1])
  {
    fill(135, 183, 84);
    rect(0, 152, tach[0]*16, 16);
    
    if (tach[0] > 15)
    {
      fill(200, 72, 72);
      rect(256, 152, (tach[0] - 16)*16, 16);
    }
    
    fill(135, 183, 84);
    rect(0, 358, tach[1]*16, 16);
    
    if (tach[1] > 15)
    {
      fill(200, 72, 72);
      rect(256, 358, (tach[1] - 16)*16, 16);
    }
  }
  else
  { 
    if (blown[0])
    {
      fill(0);
      text("BLOWN", 380, 202);
      
      if (!blownSound)
        xplosionFile.play();
      blownSound = true;
    }
    
    if (blown[1])
    {
      fill(0);
      text("BLOWN", 380, 408);
      
      if (!blownSound)
        xplosionFile.play();
      blownSound = true;
    }
  }

  if (countDown < 1)
  {
    startGame();
    
    if (playerFinish[0] == false)
    {
      counter[0] = (millis() - startTime)/1000.0;    
    }
    
    if (playerFinish[1] == false)
    {
      counter[1] = (millis() - startTime)/1000.0; 
    }
    
    textSize(16);
    fill(0);
    text(counter[0], 60, 202);
    text(counter[1], 60, 408);
  }
  else
  {
    if (early[0] || early[1])
    {
      textSize(16);
      fill(0);
     
      if (early[0])
      {
        text("EARLY", 60, 202);
        text(countDown, 60, 408);
      }
      
      if (early[1])
      {
        text(countDown, 60, 202);
        text("EARLY", 60, 408);
      }
    }
    else
    {
      textSize(16);
      fill(0);
      text(countDown, 60, 202);
      text(countDown, 60, 408);
    }
  }
  
  text(gear[0], 160, 202);
  text(gear[1], 160, 408);
  
  text(currentState[0], 220, 202);
  text(currentState[1], 220, 408);
  
  text(distance[0]/256.0, 280, 202);
  text(distance[1]/256.0, 280, 408);
  
  for (int i=0;i<2;i++)
  {
    if (speed[i] > 0)
    {
      image(dragster[dragsterFrame[i]], distance[i]/256*3.72, 55 + i*207);
      
      dragsterFrame[i]++;
        if (dragsterFrame[i] > 2)
      dragsterFrame[i] = 0;
    }
    else 
    {
      image(dragster[i], distance[i]/256*3.72, 55 + i*207);
    }
  }
} 

void keyPressed() 
{
  if (key == 'r')
  {
    early[0] = false;
    early[1] = false;
    
    blown[0] = false;
    blown[1] = false;
    
    countDown = 9;
    started = false;
    
    gear[0] = 0;
    gear[1] = 0;
    
    tach[0] = 0;
    tach[1] = 0;
    
    speed[0] = 0;
    speed[1] = 0;

    distance[0] = 0;
    distance[1] = 0;
    
    currentState[0] = 'N';
    currentState[1] = 'N';
    
    playerFinish[0] = false;
    playerFinish[1] = false;
    
    blownSound = false;
    
    for (int i=0;i<2;i++)
    {
      gearFile[i][gear[i]].stop();
    }
  }

  if (key == 'w' )
  {
    clutch[0] = true;
  }
  
  if (key == 'd')
  {
    gas[0] = true;
  }
  
  if (keyCode == UP)
  {
    clutch[1] = true;
  }
  
  if (keyCode == RIGHT)
  {
    gas[1] = true; 
  }
}

void keyReleased()
{
  if (key == 'w')
  {
    if (gear[0] < maxGear)
    {
      shift[0] = true;
    }
    
    clutch[0] = false;
  }
  
  if (key == 'd')
  {
    gas[0] = false;
  }
  
  if (keyCode == UP)
  {
    if (gear[1] < maxGear)
    {
      shift[1] = true;
    }
    
    clutch[1] = false;
  }
  
  if (keyCode == RIGHT)
  {
    gas[1] = false;
  }
}

void startGame()
{
  if (!started)
  {
    startTime = millis();
    
    started = true;
  }
}

void updateState()
{     
    for (int i=0;i<2;i++)
    {
      if (tach[i] >= 20 && gear[i] > 1)
      {
        currentMaxSpeed[i] = tach[i] * int(pow(2, gear[i] - 1)) + int(pow(2, gear[i] - 2));
      }
      else
      {
        currentMaxSpeed[i] = tach[i] * int(pow(2, gear[i] - 1));
      }
    
      if (i == 0)
      {
        if (millis() - elapsed > 200 && !started)
        {
          countDown--;
          beepFile.play();
          elapsed = millis();
        }
      }
    
      if (shift[i] && countDown > 0)
      {
         early[i] = true;
      }
    
      if (!playerFinish[i])
      {      
        if (clutch[i] || shift[i] || gas[i])
          neutral[i] = false;
        
        if (!neutral[i])
          currentState[i] = 'G'; 
          
        if (gas[i])
        {
          if (!gearFile[i][gear[i]].isPlaying())
            gearFile[i][gear[i]].play();
        }
        else
        {
          if (gearFile[i][gear[i]].isPlaying())
            gearFile[i][gear[i]].stop(); 
        }
        
        if (frameCounter == 1)
        {
          if (gas[i])
          {
            if (tach[i] >= maxTach)
            {
              blown[i] = true; 
              
              gearFile[i][gear[i]].stop();
            }
            else
            {
              if (gear[i] == 0)
              {
                tach[i] += 2;
              }
              else
              {
                tach[i]++;
              }
            }
          }
          
          if (!gas[i])
          {
            if (tach[i] > 0)
            {
              if (gear[i] == 0)
              {
                tach[i] -= 2;
              }
              else
              {
                tach[i]--;
              }
            }
          }
          
        }
        
        if (speed[i] < (currentMaxSpeed[i] - 16) && tach[i] > 0)
        {
          tach[i]--; 
        }
        
        if (i == 0)
          frameCounter++;
        if (frameCounter > frameReset[gear[0]])
        {
          frameCounter = 1;
        }
        
        if (clutch[i] == true)
        {
          currentState[i] = 'C';
          
          if (gas[i])
          {
            tach[i] += 3; 
            if (!shiftFile[i].isPlaying())
            {
              shiftFile[i].play();
            }
          }
          else
          {
            if (tach[i] > 2)
            {
              tach[i] -= 3;
            }
            else
            {
              tach[i] = 0;
            }
          }
        }
        
        if (shift[i] == true)
        {
          gearFile[i][gear[i]].stop();
          
          currentState[i] = 'S';
         
          if (gear[i] < maxGear)
            gear[i]++;
            
          shift[i] = false;
          stall[i] = true;
        }
        
        distance[i] += speed[i];
        
        if (!stall[i])
        {
          if (speed[i] < currentMaxSpeed[i])
          {
            speed[i] += 2;
          }
          else
          if (speed[i] > currentMaxSpeed[i])
          {
            speed[i]--;
          }
        }
        else
        {        
          stall[i] = false; 
        }
        
        if (distance[i] >= maxDistance)
        {
          playerFinish[i] = true; 
        }
      }
    }
}
